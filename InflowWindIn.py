from collections import OrderedDict

keys = [
    "header1",
    "line1",
    "line2",
    "Echo",
    "WindType",
    "PropagationDir",
    "NWindVel",
    "WindVxiList",
    "WindVyiList",
    "WindVziList",
    "ParametersforSteadyWindConditions",
    "HWindSpeed",
    "RefHt1",
    "PLexp",
    "ParametersforUniformwindfile",
    "FilenameUniform",
    "RefHt2",
    "RefLength",
    "ParametersforBinaryTurbSimFull-Fieldfiles",
    "FilenameBinary",
    "ParametersforBinaryBladed-styleFull-Fieldfiles",
    "FilenameRoot",
    "TowerFile",
    "ParametersforHAWC-formatbinaryfiles",
    "FileName_u",
    "FileName_v",
    "FileName_w",
    "nx",
    "ny",
    "nz",
    "dx",
    "dy",
    "dz",
    "RefHt",
    "Scalingparametersforturbulence",
    "ScaleMethod",
    "SFx",
    "SFy",
    "SFz",
    "SigmaFx",
    "SigmaFy",
    "SigmaFz",
    "Meanwindprofileparameters",
    "URef",
    "WindProfile",
    "PLExp",
    "Z0",
    "OUTPUT",
    "SumPrint",
    "OutList",
    "Wind1VelX",
    "ENDofinputfile"
]

class InflowWind:

    def __init__(self):
        try:
            with open('InflowWind.dat','r') as in_file:
                    baseInflowWind = in_file.readlines()
                    self.input = OrderedDict(zip(keys,baseInflowWind))
        except OSError as err:
            print(f"Could not open file InflowWind.dat, {err}")  

