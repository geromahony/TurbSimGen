# Vary TurbSim Input values from Excel Spreadsheet

A basic python script to read TurbSim variables from a spreadsheet and write an input file. Uses a base TurbSim input file and replaces the existing parameters in file with any found in spreadsheet as defined in row one column headers.

# Usage
The script has two command line flags:

`--input`  to specify an input spreadsheet(.xlsx), can be a relative path, i.e `../<my_spreadsheet.xlsx>`

`--output` path to an output directory 

## Example call:

```python
python GenTurbSim.py --input <my_spreadsheet.xlsx> --output "./my_output_dir"
```
